var request = require('request');
var cheerio = require('cheerio');
var async = require('async');
var jade = require('jade');
var fs = require('fs');
var _ = require('lodash');
var later = require('later');

var fetchHackernews = function(cb) {

  request('https://news.ycombinator.com/', function(error, response, body) {

    var ret = [];
    if (!error && response.statusCode == 200) {

      $ = cheerio.load(body);
      $('.title > a').each(function() {
        $a = $(this);
        var url = $a.attr('href');
        if (url.indexOf('item') === 0) {
          url = 'https://news.ycombinator.com/' + url;
        }
        ret.push({
          url: url,
          title: $a.text().trim()
        });

      });

      cb(null, ret);
    }
  })
};

var fetchDesignerNews = function(cb) {
  var ret = [];
  request('https://www.designernews.co/', function(error, response, body) {
    if (!error && response.statusCode == 200) {

      $ = cheerio.load(body);
      $('.list-story .story-details > a').each(function() {
        $a = $(this);
        var span = $a.find('span');
        span.remove();
        var url = $a.attr('href');
        if(url[0]=='/'){
            url = 'https://www.designernews.co'+url;
        }
        ret.push({
          url: url,
          title: $a.text().trim()
        });


      });
      cb(null, ret);
    }
  })
};

var fetchProductNews = function(cb) {

  var ret = [];
  request('http://www.producthunt.com/', function(error, response, body) {
    if (!error && response.statusCode == 200) {

      $ = cheerio.load(body);

      $('[data-react-class="FeaturedFeed"]').data().reactProps.post_groups.forEach(function(p) {
        p.posts.forEach(function(e) {

          ret.push({
            url: 'http://www.producthunt.com' + e.shortened_url,
            title: e.name,
            description: e.tagline
          });
        });
      });

      cb(null, ret);

    }
  })

}




var generateHTML = function() {
  async.parallel({
    hackerNews: fetchHackernews,
    designerNews: fetchDesignerNews,
    productNews: fetchProductNews
  }, function(err, result) {


    var template = jade.compileFile('jade/index.jade', {
        pretty: true
      }

    );

    result.hackerNews = _.first(result.hackerNews, 20);
    result.designerNews = _.first(result.designerNews, 20);
    result.productNews = _.first(result.productNews, 20);

    var d = new Date();
    result.time = d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    var html = template(result);


    fs.writeFile('../index.html', html, function(err) {
      if (err) throw err;
    });


  });
}


// will fire every 15 minutes
var textSched = later.parse.text('every 15 min');
// execute logTime for each successive occurrence of the text schedule
var timer = later.setInterval(generateHTML, textSched);




generateHTML();
